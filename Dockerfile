FROM maven:3-jdk-10
RUN apt-get update
COPY /target/tmobile-parser-1.0.war tmobile-utility.war
EXPOSE 8086
ENTRYPOINT ["java", "-jar", "tmobile-utility.war"]
ENV name T-Mobile-Parser	

